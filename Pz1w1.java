public class Pz1w1 {
	public static void main(String[] args) {
		//Программа 1/1 (1-ая презентация / 1-ое задание)
		java.util.Scanner in = new java.util.Scanner(System.in);
		System.out.print("Input a number - 1: ");
		int num1 = in.nextInt();
		System.out.print("Input a number - 2: ");
		int num2 = in.nextInt();
		int p1;
		int p2;
		double p3;
		double p4;
		double p5;
		p1 = num1 + num2;
		p2 = num1 - num2;
		p3 = num1 * num2;
		p4 = num1 / num2;
		p5 = num1 % num2;
		System.out.println(num1+" + "+num2+" = "+p1);
		System.out.println(num1+" - "+num2+" = "+p2);
		System.out.println(num1+" * "+num2+" = "+p3);
		System.out.println(num1+" / "+num2+" = "+p4);
		System.out.println(num1+" % "+num2+" = "+p5);
	}
}